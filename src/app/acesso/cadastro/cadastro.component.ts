import { Component, OnInit } from '@angular/core';

//expor o metodo do componente filho(cadastro) para o pai(Acesso) com Event
import { EventEmitter, Output } from '@angular/core';

//Reactive Forms
import {FormGroup, FormControl} from '@angular/forms'

//Modelo que Representa o Usuario
import {Usuario} from '../usuario.model'

//Servico de Autenticacao
import { Autenticacao } from '../../autenticacao.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  //Componente filho expondo seu EventEmitter
  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter<string>()

  //Reactive Form
  public formulario: FormGroup = new FormGroup({
    'email': new FormControl(null),
    'nome_completo': new FormControl(null),
    'nome_usuario': new FormControl(null),
    'senha': new FormControl(null)
  })

  //Autenticao sera injetado atraves do app.Module
  constructor(private autenticacao: Autenticacao) { }

  ngOnInit() {
  }

  public exibirPainelLogin():void{
    console.log('EventEmitter em cadastro => emit(login)')
    
    //Sera recebido pelo componemnte acesso
    this.exibirPainel.emit('login')
  }

  public cadastrarUsuario():void{
    console.log(this.formulario)

    let usuario: Usuario = new Usuario(
      this.formulario.value.email,
      this.formulario.value.nome_completo,
      this.formulario.value.nome_usuario,
      this.formulario.value.senha
    ) 

    this.autenticacao.cadastrarUsuario(usuario)
      .then(()=> this.exibirPainelLogin())
  }

}
