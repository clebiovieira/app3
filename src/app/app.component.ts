import { Component, OnInit} from '@angular/core';

//Firebase - Importar todos os recursos e acessar por alias
import * as firebase from 'firebase'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'InstAnimals';

  ngOnInit(): void{
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyBDLL_CTpXzVuK-34zseHd4i8SCZ7evlqk",
      authDomain: "app3-clebio.firebaseapp.com",
      databaseURL: "https://app3-clebio.firebaseio.com",
      projectId: "app3-clebio",
      storageBucket: "app3-clebio.appspot.com",
      messagingSenderId: "640807297726"
    };
    firebase.initializeApp(config);
  }
}
