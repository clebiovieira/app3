import { Component, OnInit } from '@angular/core';

//expor o metodo do componente filho(login) para o pai(Acesso) com Event
import { EventEmitter, Output } from '@angular/core';

//Reactive Forms
import {FormGroup, FormControl} from '@angular/forms'

//Servico de Autenticacao
import { Autenticacao } from '../../autenticacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //Componente filho expondo seu EventEmitter
  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter()


  //Reactive Form
  public formulario: FormGroup = new FormGroup({
    'email': new FormControl(null),
    'senha': new FormControl(null)
  })

  //Autenticao sera injetado atraves do app.Module
  constructor(private autenticacao: Autenticacao) { }

  ngOnInit() {
  }

  public exibirPainelCadastro():void{
    console.log('EventEmitter em Login => emit(cadastro)')
    
    //Sera recebido pelo componemnte acesso
    this.exibirPainel.emit('cadastro')
  }

  public autenticar(): void{
    this.autenticacao.autenticar(
      this.formulario.value.email,
      this.formulario.value.senha
    )   
  }

}
