import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { Usuario } from "./acesso/usuario.model";

//Firebase - Importar todos os recursos e acessar por alias
import * as firebase from 'firebase'

import {AutenticacaoGuard} from './autenticacao-guard.service'

@Injectable()
export class Autenticacao{    
    public token_id: string

    constructor(private router:Router){}
    
    public cadastrarUsuario(usuario : Usuario):Promise<any>{
        //Cadastrar um usuario
       return firebase.auth().createUserWithEmailAndPassword(usuario.email,usuario.senha)
            .then((resposta: any) => {
                
                //Remover atributo do objeto usuario para nao salvar a senha no banco
                delete usuario.senha

                //Gravando outras informacao no database do Firebase atraves de um path
                //passar para base64 firebase nao aceita caracter especial btoa()
                firebase.database().ref(`usuario_detalhe/${btoa(usuario.email)}`)
                    .set(usuario)
            })
            .catch((error:any)=> console.log(error))
    }

    public autenticar(email:string, senha: string):void{
        firebase.auth().signInWithEmailAndPassword(email,senha)
            .then((resposta:any)=>{
                
                //Recuperando o JWT do usuário logado
                firebase.auth().currentUser.getIdToken()
                    .then((idToken:string)=>{
                        this.token_id = idToken
                        
                        //Guardar o token no Storage do navegador
                        localStorage.setItem('idToken',idToken)

                        //Redirecionar para rota Home
                        this.router.navigate(['/home'])
                    })
            })
            .catch((error:any)=>console.log(error))
    }

    public autenticado():boolean{
        if(this.token_id === undefined && localStorage.getItem('idToken') !== null){
            this.token_id = localStorage.getItem('idToken')
        }
 
        if(this.token_id === undefined){
            //Redirecionar para raiz
            this.router.navigate(['/'])            
        }

        return this.token_id !== undefined
    }

    public sair():void{
        //Fazer logout no firebase
        firebase.auth().signOut()
            .then(()=>{
                //Remover token do navegador
                localStorage.removeItem('idToken')

                //Limpar token gravado no servico
                this.token_id = undefined

                //Redirecionar para raiz
                this.router.navigate(['/'])
            })
    }
}