import { Component, OnInit } from '@angular/core';

//Reactive Forms
import {FormGroup, FormControl} from '@angular/forms'

import { Bd } from '../../bd.service';

import * as firebase from 'firebase'

//Progresso do Upload
import { Progresso } from '../../progresso.service';

//Observable e os operadores do rxjs
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject'; //Ponto de saida para o observable time
import 'rxjs/Rx'

//EventEmitter para enviar mensagem pro componente pai
import { EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-incluir-publicacao',
  templateUrl: './incluir-publicacao.component.html',
  styleUrls: ['./incluir-publicacao.component.css']
})
export class IncluirPublicacaoComponent implements OnInit {

  public email: string
  public imagem: any

  public progressoPublicacao: string = 'pendente'
  public porcentagemUpload: number

  //Sera recuperado pelo pai(home) indicando que deve atualizar a timeline
  @Output() public atualizarTimeLine: EventEmitter<any> = new EventEmitter<any>()


  //Reactive Form
  public formulario: FormGroup = new FormGroup({
    'titulo': new FormControl(null)
  })

  constructor(private bd: Bd, private progresso: Progresso) { }

  ngOnInit() {
    //Qualquer mudanca de estado no usuario autenticado sera Observado
    firebase.auth().onAuthStateChanged((user)=>{
      this.email = user.email
    })
  }

  public publicar(): void{
    this.bd.publicar({
      email: this.email,
      titulo: this.formulario.value.titulo,
      imagem: this.imagem[0]
    })

    let acompanhamentoUpload = Observable.interval(1500)
    
    let continua = new Subject()
    continua.next(true)

    acompanhamentoUpload
    .takeUntil(continua)
    .subscribe(()=>{
      
      this.progressoPublicacao = 'andamento'
      this.porcentagemUpload = Math.round((this.progresso.estado.bytesTransferred / this.progresso.estado.totalBytes)*100)
      
      if(this.progresso.status === 'concluido'){
        continua.next(false)
        this.progressoPublicacao = 'concluido'

        //Enviar msg para componente pai (Home) ate que chegue no irmao (publicacoes)
        this.atualizarTimeLine.emit()
      }

    })

  }

  public preparaImagemUpload(event: Event):void{
    this.imagem = (<HTMLInputElement>event.target).files
  }

}
