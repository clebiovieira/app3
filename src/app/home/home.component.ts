import { Component, OnInit } from '@angular/core';
import { Autenticacao } from '../autenticacao.service';

//Ligar o componente pai ao filho atraves de um atributo
import { ViewChild } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  //Link com o atributo #publicacoes em home
  @ViewChild('publicacoesViewChild') public publicacoes: any

  constructor(private autenticacao:Autenticacao) { }

  ngOnInit() {
  }

  public sair():void{
    this.autenticacao.sair()
  }
  
  public atualizarTimeLinePai(): void{
    console.log('pai recebeu EventEmitter do app-incluir-publicacao')
    
    //Atraves do ViewChild acesso o metodo no filho publicacoesComponent
    this.publicacoes.atualizarTimeLine()
  }
}
